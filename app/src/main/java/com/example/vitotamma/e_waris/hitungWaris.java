package com.example.vitotamma.e_waris;

import java.util.Arrays;

public class hitungWaris {

    int []ahliWaris;
    int []bagAhliWaris = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    int []furudA = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    int []furudB = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    int []asobah = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    String []dftAhliWaris = {"Anak laki-laki","Anak perempuan","Cucu laki-laki dari anak laki-laki",
            "Cucu perempuan dari anak laki-laki","Bapak","Ibu","Suami","Istri","Kakek","Nenek (ibu dari bapak)",
            "Nenek (ibu dari ibu)","Nenek (ibu dari kakek)","Saudara kandung","Saudari kandung","Saudara sebapak",
            "Saudari sebapak","Saudara/i seibu","Putra dari saudara kandung","Saudara dari saudara sebapak",
            "Paman kandung","Paman sebapak","Putra dari paman kandung","Putra dari paman sebaak","Pria yang memerdekakan budak","Wanita yang memerdekakan budak"};
    //    private int countFurud=0;
    int asalMasalah;
    int totalHarta;
    int sisa;
    int bagian;
    /*
    0  -> Anak lk*
    1  -> Anak pr*
    2  -> Cucu lk dari anak lk*
    3  -> Cucu pr dari anak lk*
    4  -> Bapak
    5  -> Ibu
    6  -> Suami
    7  -> Istri*
    8  -> Kakek
    9  -> Nenek (ibu dari bapak)
    10 -> Nenek (ibu dari ibu)
    11 -> Nenek (ibu dari kakek)
    12 -> Saudara kandung*
    13 -> Saudari kandung*
    14 -> Saudara sebapak*
    15 -> Saudari sebapak*
    16 -> Saudara/i seibu*
    17 -> Putra dari sdr kandung*
    18 -> Putra dari sdr sebapak*
    19 -> Paman sekandung*
    20 -> Paman sebapak*
    21 -> Putra dari paman kandung*
    22 -> Putra dari paman sebapak*
    23 -> Pria yg memerdekakan budak
    24 -> Wanita yg memerdekakan budak
    */

    public hitungWaris(int []ahliWaris,int total) {
        this.ahliWaris = ahliWaris;
        totalHarta = total;

//        this.furudA = new int[26];

        sisa = totalHarta;
    }

    public void Harta(int total, int wasiat, int hutang, int tahjiz){
        totalHarta = total-(wasiat+hutang+tahjiz);
    }

    public int getHarta(){
        return totalHarta;
    }

    public int []getBagian(){
        return bagAhliWaris;
    }

    public int []getFurudA(){
        return furudA;
    }

    public int []getFurudB(){
        return furudB;
    }

    public int []getAsobah(){
        return asobah;
    }

    public int getAsalMasalah(){
        return asalMasalah;
    }

    public void pembagian(){
        int hartaTerbagi=0;
        if((ahliWaris[4]>0&&ahliWaris[5]>0)&&(ahliWaris[6]>0||ahliWaris[7]>0)&&(ahliWaris[0]==0&&ahliWaris[1]==0&&ahliWaris[2]==0&&ahliWaris[3]==0&&furudA[8]==0&&furudA[9]==0&&furudA[10]==0&&furudA[11]==0&&furudA[12]==0&&furudA[13]==0&&furudA[14]==0&&furudA[15]==0&&furudA[16]==0&&furudA[17]==0&&furudA[18]==0&&furudA[19]==0&&furudA[20]==0&&furudA[21]==0&&furudA[22]==0&&furudA[23]==0&&furudA[24]==0)){
            System.out.println("Ghorowain");
            ghorowain();
        }
        else{
            if(asalMasalah!=0){
                bagian=totalHarta/asalMasalah;
                for(int i=0;i<bagAhliWaris.length;i++){
                    if(ahliWaris[i]!=0){
                        bagAhliWaris[i]=bagian*furudA[i]/ahliWaris[i];
                    }
                }
                hartaTerbagi=0;
                for(int i=0;i<bagAhliWaris.length;i++){
                    hartaTerbagi+=(bagAhliWaris[i]*ahliWaris[i]);
                }
                sisa-=hartaTerbagi;
            }
        }
        System.out.println("Harta terbagi = "+hartaTerbagi);
    }

    public void asalMasalah(){
//        boolean cek=true;
        int temp[] = new int[26];
        for(int i=0;i<furudB.length;i++){
            temp[i]=furudB[i];
        }
        Arrays.sort(temp);
//        System.out.println("temp 0 "+temp[0]);
        int ind=0;
        for(int i=0;i<temp.length;i++){
//            System.out.println("Temp "+i+" = "+temp[i]);
            if(temp[i]!=0){
                ind=temp[i];
                break;
            }
        }
        boolean cek=true;
        int kpk=ind;
        do{
            kpk+=ind;
            for(int i=0;i<temp.length;i++){
                if(temp[i]!=0){
                    if(kpk%temp[i]!=0){
                        cek=false;
                        break;
                    }
                    else
                        cek=true;
                }
            }
        }while(cek==false);
        System.out.println("Asal Masalah = "+kpk);
        for(int i=0;i<furudA.length;i++){
            if(furudA[i]!=0&&furudB[i]!=0){
                if(furudB[i]!=kpk){
                    furudA[i]=furudA[i]*(kpk/furudB[i]);
                }
            }
        }
        asalMasalah=kpk;
    }
    public void ghorowain(){
        if(ahliWaris[6]>0){
            bagAhliWaris[6]=totalHarta/2;
            sisa-=bagAhliWaris[6];
            bagAhliWaris[5]=totalHarta/6;
            furudA[5]=1;
            sisa-=bagAhliWaris[5];
            bagAhliWaris[4]=sisa;
            furudA[4]=2;
            asobah[4]=0;
            sisa-=bagAhliWaris[4];
        }
        if(ahliWaris[7]>0){
            bagAhliWaris[7]=totalHarta/4/ahliWaris[7];
            sisa-=bagAhliWaris[7]*ahliWaris[7];
            bagAhliWaris[5]=totalHarta/4;
            furudA[5]=3;
            sisa-=bagAhliWaris[5];
            bagAhliWaris[4]=sisa;
            furudA[4]=6;
            asobah[4]=0;
            sisa-=bagAhliWaris[4];
        }
    }
    public void dispBagian(){
        for(int i=0;i<dftAhliWaris.length;i++){
            if(bagAhliWaris[i]!=0){
                if(furudA[i]!=0){
                    if((i==4&&asobah[i]>0)||i==8&&asobah[i]>0){
                        System.out.println(dftAhliWaris[i]+" ("+furudA[i]+"/"+asalMasalah+"+sisa) = "+bagAhliWaris[i]);
                    }
                    else{
                        System.out.println(dftAhliWaris[i]+" ("+furudA[i]+"/"+asalMasalah+") = "+bagAhliWaris[i]);
                    }
                }
                else{
                    System.out.println(dftAhliWaris[i]+" (sisa) = "+bagAhliWaris[i]);
                }
            }
        }
    }
    public void dispFurudB(){
        for(int i=0;i<furudB.length;i++){
            System.out.print(furudB[i]+" ");
        }
    }
    public void dispFurudA(){
        for(int i=0;i<furudA.length;i++){
            System.out.print(furudA[i]+" ");
        }
    }
    public void dispAsobah(){
        for(int i=0;i<asobah.length;i++){
            System.out.print(asobah[i]+" ");
        }
    }
    public void bagiSisa(){
        int sumAsobah=0;
        for(int i=0;i<asobah.length;i++){
            sumAsobah+=asobah[i];
        }
        if(sumAsobah>0){
            bagian=sisa/sumAsobah;
            for(int i=0;i<bagAhliWaris.length;i++){
                if(ahliWaris[i]!=0){
                    bagAhliWaris[i]+=asobah[i]*bagian/ahliWaris[i];
                }
            }
        }
    }
    public void bagiFurud(){
        bagian=totalHarta/asalMasalah;
        for(int i=0;i<bagAhliWaris.length;i++){

        }
    }
    //Anak lk
    public void bagian0(){
        if(ahliWaris[0]>0){
            if(ahliWaris[1]==0){
                asobah[0]=1*ahliWaris[0];
            }
            else{
                asobah[0]=2*ahliWaris[0];
            }
        }
    }
    //Anak pr
    public void bagian1(){
        if(ahliWaris[1]>0){
            if(ahliWaris[0]==0){
                if(ahliWaris[1]>1){
                    furudA[1]=2;
                    furudB[1]=3;
                }
                else{
                    furudA[1]=1;
                    furudB[1]=2;
                }
            }
            else
                asobah[1]=1*ahliWaris[1];
        }
    }

    public void dispSisa(){
        System.out.println("Sisa = "+sisa);
    }
    //cucu lk
    public void bagian2(){
        if(ahliWaris[0]==0){
            if(ahliWaris[2]>0){
                if(ahliWaris[3]==0){
                    asobah[2]=1;
                }
                else{
                    if(ahliWaris[1]!=0){
                        asobah[2]=1;
                    }
                    else{
                        asobah[2]=2;
                    }
                }
            }
        }
    }
    //cucu pr
    public void bagian3(){
        if(ahliWaris[3]>0){
            if(ahliWaris[0]==0&&ahliWaris[2]==0){
                if(ahliWaris[1]==0){
                    furudA[3]=2;
                    furudB[3]=3;
                }
                else{
                    if(ahliWaris[1]<2){
                        furudA[3]=1;
                        furudB[3]=6;
                    }
                }
            }
            else if(ahliWaris[0]==0){
                if(ahliWaris[2]==0){
                    if(ahliWaris[3]==1){
                        furudA[3]=1;
                        furudB[3]=2;
                    }
                    else{
                        furudA[3]=2;
                        furudB[3]=3;
                    }
                }
                else
                    asobah[3]=1;
            }
        }
    }
    //bapak
    public void bagian4(){
        if(ahliWaris[4]>0){
            if(ahliWaris[0]>0||ahliWaris[2]>0){
                furudA[4]=1;
                furudB[4]=6;
            }
            else{
                if(ahliWaris[1]>0||ahliWaris[3]>0){
                    furudA[4]=1;
                    furudB[4]=6;
                    asobah[4]=1;
                }
                else
                    asobah[4]=1;
            }
        }
    }
    //ibu
    public void bagian5(){
        if(ahliWaris[5]>0){
            if(ahliWaris[0]>0||ahliWaris[1]>0||ahliWaris[2]>0||ahliWaris[3]>0||ahliWaris[12]>1||ahliWaris[13]>1||(ahliWaris[12]>0&&ahliWaris[13]>0)){
                furudA[5]=1;
                furudB[5]=6;
            }
            else{
                furudA[5]=1;
                furudB[5]=3;
            }
        }
    }
    //suami
    public void bagian6(){
        if(ahliWaris[6]>0){
            if(ahliWaris[0]>0||ahliWaris[1]>0||ahliWaris[2]>0||ahliWaris[3]>0){
                furudA[6]=1;
                furudB[6]=4;
            }
            else{
                furudA[6]=1;
                furudB[6]=2;
            }
        }
    }
    //istri
    public void bagian7(){
        if(ahliWaris[7]>0){
            if(ahliWaris[0]>0||ahliWaris[1]>0||ahliWaris[2]>0||ahliWaris[3]>0){
                furudA[7]=1;
                furudB[7]=8;
            }
            else{
                furudA[7]=1;
                furudB[7]=4;
            }
        }
    }
    //kakek
    public void bagian8(){
        if(ahliWaris[8]>0){
            if(ahliWaris[4]==0){
                if(ahliWaris[0]>0||ahliWaris[2]>0){
                    furudA[8]=1;
                    furudB[8]=6;
                }
                else{
                    if(ahliWaris[1]>0||ahliWaris[3]>0){
                        furudA[8]=1;
                        furudB[8]=6;
                        asobah[8]=1;
                    }
                    else{
                        asobah[8]=1;
                    }
                }
            }
        }
    }
    //nenek (ibu dari bapak)
    public void bagian9(){
        if(ahliWaris[9]>0){
            if(ahliWaris[5]==0){
                if(ahliWaris[10]==0){
                    furudA[9]=1;
                    furudB[9]=6;
                }
                else{
                    furudA[9]=1;
                    furudB[9]=12;
                }
            }
        }
    }
    //nenek (ibu dari ibu)
    public void bagian10(){
        if(ahliWaris[10]>0){
            if(ahliWaris[5]==0){
                if(ahliWaris[9]==0){
                    furudA[10]=1;
                    furudB[10]=6;
                }
                else{
                    furudA[10]=1;
                    furudB[10]=12;
                }
            }
        }
    }
    //nenek (ibu dari kakek)
    public void bagian11(){
        if(ahliWaris[11]>0){
            if(ahliWaris[5]==0){
                if(ahliWaris[9]==0&&ahliWaris[10]==0){
                    furudA[11]=1;
                    furudB[11]=6;
                }
            }
        }
    }
    //saudara kandung
    public void bagian12(){
        if(ahliWaris[12]>0){
            if(ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0){
                if(ahliWaris[13]>0)
                    asobah[12]=2;
                else
                    asobah[12]=1;
            }
        }
    }
    //saudari kandung
    public void bagian13(){
        if(ahliWaris[13]>0){
            if(ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[8]==0){
                if(ahliWaris[12]==0){
                    if(ahliWaris[1]==0&&ahliWaris[3]==0){
                        if(ahliWaris[13]>1){
                            furudA[13]=2;
                            furudB[13]=3;
                        }
                        else{
                            furudA[13]=1;
                            furudB[13]=2;
                        }
                    }
                    else{
                        asobah[13]=1;
                    }
                }
                else{
                    asobah[13]=1;
                }
            }
        }
    }
    //saudara sebapak
    public void bagian14(){
        if(ahliWaris[14]>0){
            if(ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[12]==0&&(ahliWaris[13]==0&&ahliWaris[1]==0)&&(ahliWaris[13]==0&&ahliWaris[3]==0)){
                if(ahliWaris[15]>0)
                    asobah[15]=2;
                else
                    asobah[15]=1;
            }
        }
    }
    //saudari sebapak
    public void bagian15(){
        if(ahliWaris[15]>0){
            if(ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[12]==0&&(ahliWaris[13]==0&&ahliWaris[1]==0)&&(ahliWaris[13]==0&&ahliWaris[3]==0)){
                if(ahliWaris[13]==1){
                    furudA[15]=1;
                    furudB[15]=6;
                }
                if(ahliWaris[14]==0&&ahliWaris[1]==0&&ahliWaris[3]==0){
                    if(ahliWaris[15]==1){
                        furudA[15]=1;
                        furudB[15]=2;
                    }
                    else{
                        furudA[15]=2;
                        furudB[15]=3;
                    }
                }
                else{
                    if(ahliWaris[14]>0){
                        asobah[15]=1;
                    }
                    if(ahliWaris[3]>0){
                        asobah[15]=1;
                    }
                    if(ahliWaris[1]>0){
                        asobah[15]=1;
                    }
                }
            }
        }
    }
    //saudara/i seibu
    public void bagian16(){
        if(ahliWaris[16]>0){
            if(ahliWaris[0]==0&&ahliWaris[1]==0&&ahliWaris[2]==0&&ahliWaris[3]==0&&ahliWaris[4]==0&&ahliWaris[0]==8) {
                if (ahliWaris[16] > 1) {
                    furudA[16] = 1;
                    furudB[16] = 3;
                } else {
                    furudA[16] = 1;
                    furudB[16] = 6;
                }
            }
        }
    }
    //putra dari sdr kandung
    public void bagian17(){
        if(ahliWaris[17]>0){
            if(ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[8]==0&&ahliWaris[12]==0&&ahliWaris[14]==0){
                asobah[17]=1;
            }
        }
    }
    //putra dari sdr sebapak
    public void bagian18(){
        if(ahliWaris[18]>0){
            if(ahliWaris[17]==0&&ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[8]==0&&ahliWaris[12]==0&&ahliWaris[14]==0){
                asobah[18]=1;
            }
        }
    }
    //paman sekandung
    public void bagian19(){
        if(ahliWaris[19]>0){
            if(ahliWaris[17]==0&&ahliWaris[18]==0&&ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[8]==0&&ahliWaris[12]==0&&ahliWaris[14]==0){
                asobah[19]=1;
            }
        }
    }
    //paman sebapak
    public void bagian20(){
        if(ahliWaris[20]>0){
            if(ahliWaris[17]==0&&ahliWaris[18]==0&&ahliWaris[19]==0&&ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[8]==0&&ahliWaris[12]==0&&ahliWaris[14]==0){
                asobah[20]=1;
            }
        }

    }
    //putra dari paman kandung
    public void bagian21(){
        if(ahliWaris[21]>0){
            if(ahliWaris[20]==0&&ahliWaris[17]==0&&ahliWaris[18]==0&&ahliWaris[19]==0&&ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[8]==0&&ahliWaris[12]==0&&ahliWaris[14]==0){
                asobah[21]=1;
            }
        }
    }
    //putra dari paman sebapak
    public void bagian22(){
        if(ahliWaris[22]>0){
            if(ahliWaris[21]==0&&ahliWaris[20]==0&&ahliWaris[17]==0&&ahliWaris[18]==0&&ahliWaris[19]==0&&ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[8]==0&&ahliWaris[12]==0&&ahliWaris[14]==0){
                asobah[22]=1;
            }
        }
    }
    //pria yg memerdekakan budak
    public void bagian23(){
        if(ahliWaris[23]>0){
            if(ahliWaris[22]==0&&ahliWaris[21]==0&&ahliWaris[20]==0&&ahliWaris[17]==0&&ahliWaris[18]==0&&ahliWaris[19]==0&&ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[8]==0&&ahliWaris[12]==0&&ahliWaris[14]==0){
                asobah[23]=1;
            }
        }
    }
    //wanita yg memerdekakan budak
    public void bagian24(){
        if(ahliWaris[24]>0){
            if(ahliWaris[22]==0&&ahliWaris[22]==0&&ahliWaris[21]==0&&ahliWaris[20]==0&&ahliWaris[17]==0&&ahliWaris[18]==0&&ahliWaris[19]==0&&ahliWaris[0]==0&&ahliWaris[2]==0&&ahliWaris[4]==0&&ahliWaris[8]==0&&ahliWaris[12]==0&&ahliWaris[14]==0){
                asobah[24]=1;
            }
        }
    }
}
