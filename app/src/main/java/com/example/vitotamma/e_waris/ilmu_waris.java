package com.example.vitotamma.e_waris;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ilmu_waris extends AppCompatActivity {
    ArrayAdapter<String> adapter;
    ListView listView;
    public String[] waris = {"PENGERTIAN WARIS", "RUKUN WARIS", "DALIL DALIL", "BAGIAN BAGIAN AHLI WARIS", "VIDEO PENJELASAN"};
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ilmu_waris);

        listView = (ListView) findViewById(R.id.lst1);
        adapter = new ArrayAdapter<String>(this, R.layout.listview, waris);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                if (String.valueOf(listView.getAdapter().getItem(i)).equals("PENGERTIAN WARIS")) {
                    Intent intent = new Intent(ilmu_waris.this, pengertian_waris.class);
                    startActivity(intent);
                }

                if (String.valueOf(listView.getAdapter().getItem(i)).equals("RUKUN WARIS")) {
                    Intent intent = new Intent(ilmu_waris.this, rukun_waris.class);
                    startActivity(intent);
                }
                if (String.valueOf(listView.getAdapter().getItem(i)).equals("DALIL DALIL")) {
                    Intent intent = new Intent(ilmu_waris.this, dalil_dalil.class);
                    startActivity(intent);
                }
                if (String.valueOf(listView.getAdapter().getItem(i)).equals("BAGIAN BAGIAN AHLI WARIS")) {
                    Intent intent = new Intent(ilmu_waris.this, bagian_ahli_waris.class);
                    startActivity(intent);
                }
                if (String.valueOf(listView.getAdapter().getItem(i)).equals("VIDEO PENJELASAN")) {
                    Intent intent = new Intent(ilmu_waris.this, videoYoutube.class);
                    startActivity(intent);
                }
            }
        });

    }
}