package com.example.vitotamma.e_waris;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class form_perempuan extends AppCompatActivity {

    int bagi [] = new int[25];
    int harta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_perempuan);

        Intent intent = getIntent();
        this.harta = intent.getIntExtra("total harta",0);

        Log.i("total harta", "total harta "+ this.harta);
    }
    public void hitungPerempuan (View view){

        EditText anakLakinya = findViewById(R.id.ETAnakLakiP);
        String anakLaki = (anakLakinya.getText() == null ? null : anakLakinya.getText().toString());
        if (TextUtils.isEmpty(anakLaki)){
            bagi[0] = 0;
        }else {
            bagi[0] = Integer.parseInt(anakLakinya.getText().toString());
        }

        EditText anakPerempuannya = findViewById(R.id.ETAnakPermenpuanP);
        String anakPerempuan = (anakPerempuannya.getText() == null ? null : anakPerempuannya.getText().toString());
        if (TextUtils.isEmpty(anakPerempuan)){
            bagi[1] = 0;
        }else {
            bagi[1] = Integer.parseInt(anakPerempuannya.getText().toString());
        }

        EditText cucuLakinya = findViewById(R.id.ETCucuLakiP);
        String cucuLaki = (cucuLakinya.getText() == null ? null : cucuLakinya.getText().toString());
        if (TextUtils.isEmpty(cucuLaki)){
            bagi[2] = 0;
        }else {
            bagi[2] = Integer.parseInt(cucuLakinya.getText().toString());
        }

        EditText cucuPerempuannya = findViewById(R.id.ETCucuPerempuanP);
        String cucuPerempuan = (cucuPerempuannya.getText() == null ? null : cucuPerempuannya.getText().toString());
        if (TextUtils.isEmpty(cucuPerempuan)){
            bagi[3] = 0;
        }else {
            bagi[3] = Integer.parseInt(cucuPerempuannya.getText().toString());
        }

        CheckBox ayah = findViewById(R.id.CBAyahP);
        if (ayah.isChecked()){
            bagi[4]= 1;
        } else bagi[4] = 0;

        CheckBox ibu = findViewById(R.id.CBIbuP);
        if (ibu.isChecked()){
            bagi[5]= 1;
        } else bagi[5] = 0;


        CheckBox suami = findViewById(R.id.CBSuamiP);
        if (suami.isChecked()){
            bagi[6]= 1;
        } else bagi[6] = 0;

        bagi [7] = 0; //istri auto kosong

        CheckBox kakek = findViewById(R.id.CBKakekP);
        if (kakek.isChecked()){
            bagi[8]= 1;
        } else bagi[8] = 0;

        CheckBox nenekAyah = findViewById(R.id.CBNenekAyahP);
        if (nenekAyah.isChecked()){
            bagi[9]= 1;
        } else bagi[9] = 0;

        CheckBox nenekIbu = findViewById(R.id.CBNenekIbuP);
        if (nenekIbu.isChecked()){
            bagi[10]= 1;
        } else bagi[10] = 0;

        CheckBox nenekKakek = findViewById(R.id.CBNenekKakekP);
        if (nenekKakek.isChecked()){
            bagi[11]= 1;
        } else bagi[11] = 0;

        EditText saudaraLakinya = findViewById(R.id.ETSaudaraLakiKandungP);
        String saudaraLaki = (saudaraLakinya.getText() == null ? null : saudaraLakinya.getText().toString());
        if (TextUtils.isEmpty(saudaraLaki)){
            bagi[12] = 0;
        }else {
            bagi[12] = Integer.parseInt(saudaraLakinya.getText().toString());
        }

        EditText saudaraPerempuannya = findViewById(R.id.ETSaudaraPerempuanKandungP);
        String saudaraPerempuan = (saudaraPerempuannya.getText() == null ? null : saudaraPerempuannya.getText().toString());
        if (TextUtils.isEmpty(saudaraPerempuan)){
            bagi[13] = 0;
        }else {
            bagi[13] = Integer.parseInt(saudaraPerempuannya.getText().toString());
        }

        EditText saudaraLakiBapaknya = findViewById(R.id.ETSaudaraLakiSebapakP);
        String saudaraLakiBapak = (saudaraLakiBapaknya.getText() == null ? null : saudaraLakiBapaknya.getText().toString());
        if (TextUtils.isEmpty(saudaraLakiBapak)){
            bagi[14] = 0;
        }else {
            bagi[14] = Integer.parseInt(saudaraLakiBapaknya.getText().toString());
        }

        EditText saudaraPerempuanBapaknya = findViewById(R.id.ETSaudaraPerempuanSebapakL);
        String saudaraPerempuanBapak = (saudaraPerempuanBapaknya.getText() == null ? null : saudaraPerempuanBapaknya.getText().toString());
        if (TextUtils.isEmpty(saudaraPerempuanBapak)){
            bagi[15] = 0;
        }else {
            bagi[15] = Integer.parseInt(saudaraPerempuanBapaknya.getText().toString());
        }

        EditText saudaraLakiPerempuanIbunya = findViewById(R.id.ETSaudaraLakiDanPerempuanSeibuP);
        String saudaraLakiPerempuanIbu = (saudaraLakiPerempuanIbunya.getText() == null ? null : saudaraLakiPerempuanIbunya.getText().toString());
        if (TextUtils.isEmpty(saudaraLakiPerempuanIbu)){
            bagi[16] = 0;
        }else {
            bagi[16] = Integer.parseInt(saudaraLakiPerempuanIbunya.getText().toString());
        }

        EditText keponakanLakinya = findViewById(R.id.ETKeponakanLakiSaudaraKandungP);
        String keponakanLaki = (keponakanLakinya.getText() == null ? null : keponakanLakinya.getText().toString());
        if (TextUtils.isEmpty(keponakanLaki)){
            bagi[17] = 0;
        }else {
            bagi[17] = Integer.parseInt(keponakanLakinya.getText().toString());
        }

        EditText keponakanLakiAyahnya = findViewById(R.id.ETKeponakanLakiSaudaraSeayahP);
        String keponakanLakiAyah = (keponakanLakiAyahnya.getText() == null ? null : keponakanLakiAyahnya.getText().toString());
        if (TextUtils.isEmpty(keponakanLakiAyah)){
            bagi[18] = 0;
        }else {
            bagi[18] = Integer.parseInt(keponakanLakiAyahnya.getText().toString());
        }

        EditText pamannya = findViewById(R.id.ETPamanKandungP);
        String paman = (pamannya.getText() == null ? null : pamannya.getText().toString());
        if (TextUtils.isEmpty(paman)){
            bagi[19] = 0;
        }else {
            bagi[19] = Integer.parseInt(pamannya.getText().toString());
        }

        EditText pamanAyahnya = findViewById(R.id.ETPamanSebapakP);
        String pamanAyah = (pamanAyahnya.getText() == null ? null : pamanAyahnya.getText().toString());
        if (TextUtils.isEmpty(pamanAyah)){
            bagi[20] = 0;
        }else {
            bagi[20] = Integer.parseInt(pamanAyahnya.getText().toString());
        }

        EditText putraPamannya = findViewById(R.id.ETPutraPamanKandungP);
        String putraPaman = (putraPamannya.getText() == null ? null : putraPamannya.getText().toString());
        if (TextUtils.isEmpty(putraPaman)){
            bagi[21] = 0;
        }else {
            bagi[21] = Integer.parseInt(putraPamannya.getText().toString());
        }

        EditText putraPamanAyahnya = findViewById(R.id.ETPutraPamanSebapakP);
        String putraPamanAyah = (putraPamanAyahnya.getText() == null ? null : putraPamanAyahnya.getText().toString());
        if (TextUtils.isEmpty(putraPamanAyah)){
            bagi[22] = 0;
        }else {
            bagi[22] = Integer.parseInt(putraPamanAyahnya.getText().toString());
        }

        CheckBox PriaMerdekaBudak = findViewById(R.id.CBPriaMemerdekaBudakP);
        if (PriaMerdekaBudak.isChecked()){
            bagi[23]= 1;
        } else bagi[23] = 0;

        CheckBox WanitaMerdekaBudak = findViewById(R.id.CBWanitaMemerdekaBudakP);
        if (WanitaMerdekaBudak.isChecked()){
            bagi[24]= 1;
        } else bagi[24] = 0;

        hitungWaris hitung = new hitungWaris(bagi,harta);
        hitung.bagian0();
        hitung.bagian1();
        hitung.bagian2();
        hitung.bagian3();
        hitung.bagian4();
        hitung.bagian5();
        hitung.bagian6();
        hitung.bagian7();
        hitung.bagian8();
        hitung.bagian9();
        hitung.bagian10();
        hitung.bagian11();
        hitung.bagian12();
        hitung.bagian13();
        hitung.bagian14();
        hitung.bagian15();
        hitung.bagian16();
        hitung.bagian17();
        hitung.bagian18();
        hitung.bagian19();
        hitung.bagian20();
        hitung.bagian21();
        hitung.bagian22();
        hitung.bagian23();
        hitung.bagian24();
        hitung.asalMasalah();
        hitung.pembagian();
        hitung.bagiSisa();

        int []bagian=hitung.getBagian();
        int []furudA=hitung.getFurudA();
        int []furudB=hitung.getFurudB();
        int []asobah=hitung.getAsobah();
        int asalMasalah=hitung.getAsalMasalah();

        Intent intent = new Intent(this, Hasil.class);
        intent.putExtra("jumlah harta", this.harta);
        intent.putExtra("jenis kelamin", "perempuan");
        intent.putExtra("bagian", bagian);
        intent.putExtra("array bagian a", furudA);
        intent.putExtra("array bagian b", furudB);
        intent.putExtra("array asobah", asobah);
        intent.putExtra("asal",asalMasalah);
        startActivity(intent);

        //ngecek isi array
        for (int j=0; j<25;j++){
            Log.i("array", "isi array "+j+"=" + bagian[j]);
        }

        for (int k = 0; k<25; k++){
            Log.i("array pembagian", "isi array pembagian "+k+"=" + furudA[k]+"/"+furudB[k]);
        }

        for (int l = 0; l<25; l++){
            Log.i("array asobah", "isi array asobah "+l+"=" + asobah[l]);
        }


    }
}
