package com.example.vitotamma.e_waris;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class awal extends AppCompatActivity {
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    int harta;
    int hutang;
    int urusJenazah;
    int wasiat;
    int totalHarta;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_awal);
    }

    public void hitung (View view){
        radioGroup = findViewById(R.id.RG);
        int selectedId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(selectedId);

        EditText jumlahHarta = findViewById(R.id.jumlahHarta);
        String hartaWaris = (jumlahHarta.getText() == null ? null : jumlahHarta.getText().toString());
        if (TextUtils.isEmpty(hartaWaris)){
            this.harta = 0;
        }else {
            this.harta = Integer.parseInt(jumlahHarta.getText().toString());
        }

        EditText jumlahHutang = findViewById(R.id.jumlahHutang);
        String hutangWaris = (jumlahHutang.getText() == null ? null : jumlahHutang.getText().toString());
        if (TextUtils.isEmpty(hutangWaris)){
            this.hutang = 0;
        }else {
            this.hutang = Integer.parseInt(jumlahHutang.getText().toString());
        }

        EditText jumlahUrusJenazah = findViewById(R.id.jumlahUrusJenazah);
        String urusJenazahWaris = (jumlahUrusJenazah.getText() == null ? null : jumlahUrusJenazah.getText().toString());
        if (TextUtils.isEmpty(urusJenazahWaris)){
            this.urusJenazah = 0;
        }else {
            this.urusJenazah = Integer.parseInt(jumlahUrusJenazah.getText().toString());
        }


        EditText jumlahWasiat =  findViewById(R.id.jumlahWasiat);
        String wasiatWaris = (jumlahWasiat.getText() == null ? null : jumlahWasiat.getText().toString());
        if (TextUtils.isEmpty(wasiatWaris)){
            this.wasiat = 0;
        }else {
            this.wasiat = Integer.parseInt(jumlahWasiat.getText().toString());
        }

        int totalHartaTanpaWasiat = harta-hutang-urusJenazah;


        if (wasiat <= (harta/3)){
            int totalHartaDenganWasiat = totalHartaTanpaWasiat-wasiat;
            this.totalHarta=totalHartaDenganWasiat;
            if(totalHarta<0){
                Toast.makeText(getApplicationContext(),"Tidak Ada Harta yang Dibagikan",Toast.LENGTH_SHORT).show();
            }else {
                if (selectedId == R.id.RBPria) {
                    Intent intent = new Intent(this, FormLaki.class);
                    intent.putExtra("total harta", this.totalHarta);
                    startActivity(intent);
                }
                if (selectedId == R.id.RBWanita) {
                    Intent intent = new Intent(this, form_perempuan.class);
                    intent.putExtra("total harta", this.totalHarta);
                    startActivity(intent);
                }
            }
        }else {
            Toast.makeText(getApplicationContext(),"Wasiat melebihi 1/3 TOTAL HARTA",Toast.LENGTH_SHORT).show();
        }


    }
}
