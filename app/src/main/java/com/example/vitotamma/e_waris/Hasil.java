package com.example.vitotamma.e_waris;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

public class Hasil extends AppCompatActivity {

    int hartaYangDibagikan;
    String jenisKelamin;
    int []furudA;
    int []furudB;
    int []asobah;
    int []bagian;
    int asal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil);

        Intent intent = getIntent();
        this.hartaYangDibagikan = intent.getIntExtra("jumlah harta",0);
        this.jenisKelamin = intent.getStringExtra("jenis kelamin");
        this.bagian = intent.getIntArrayExtra("bagian");
        this.furudA = intent.getIntArrayExtra("array bagian a");
        this.furudB = intent.getIntArrayExtra("array bagian b");
        this.asobah = intent.getIntArrayExtra("array asobah");
        this.asal = intent.getIntExtra("asal", 0);



        if (this.jenisKelamin.equals("laki") ){
           TextView pasangan = findViewById(R.id.TVPasangan);
           pasangan.setText("Istri");
        }

        TextView totalHarta = findViewById(R.id.TVJumlahHartaBagi);
        totalHarta.setText(""+this.hartaYangDibagikan);

        TextView bAyah = findViewById(R.id.ETBAyah);
        if(furudA[4] != 0){
            if(asobah[4]>0){
                bAyah.setText(furudA[4] + "/" + asal+"+ sisa");
            }
            else {
                bAyah.setText(furudA[4] + "/" + asal);
            }
        }else{
            if(asobah[4]>0) {
                bAyah.setText("Sisa");
            }
        }
        TextView hAyah = findViewById(R.id.ETHAyah);
        hAyah.setText(bagian[4]+"");

        TextView bIbu = findViewById(R.id.ETBIbu);
        if(furudA[5]>0){
            bIbu.setText(furudA[5]+"/"+asal);
        }else bIbu.setText("-");

        TextView hIbu = findViewById(R.id.ETHIbu);
        hIbu.setText(bagian[5]+"");

        TextView bKakek = findViewById(R.id.ETBKakek);
        if (asobah[8] > 0){
            bKakek.setText("sisa");
        }else {
            if (furudA[8] > 0) {
                bKakek.setText(furudA[8] + "/" + asal);
            }else {
                bKakek.setText("-");
            }
        }
        TextView hKakek = findViewById(R.id.ETHKakek);
        hKakek.setText(bagian[8]+"");

        TextView bNenekAyah = findViewById(R.id.ETBNenekAyah);
        if(furudA[9]>0){
            bNenekAyah.setText(furudA[9]+"/"+asal);
        }else bNenekAyah.setText("-");
        TextView hNenekAyah = findViewById(R.id.ETHNenekAyah);
        hNenekAyah.setText(bagian[9]+"");

        TextView bNenekIbu = findViewById(R.id.ETBNenekIbu);
        if(furudA[10]>0){
            bNenekIbu.setText(furudA[10]+"/"+asal);
        }else bNenekIbu.setText("-");
        TextView hNenekIbu = findViewById(R.id.ETHNenekIbu);
        hNenekIbu.setText(bagian[10]+"");

        TextView bNenekKakek = findViewById(R.id.ETBNenekKakek);
        if(furudA[11]>0){
            bNenekKakek.setText(furudA[11]+"/"+asal);
        }else bNenekKakek.setText("-");
        TextView hNenekKakek = findViewById(R.id.ETHNenekKakek);
        hNenekKakek.setText(bagian[11]+"");

        if (this.jenisKelamin.equals("laki")) {
            TextView bPasangan = findViewById(R.id.ETBPasangan);
            if(furudA[7]>0){
                bPasangan.setText(furudA[7]+"/"+asal);
            }else bPasangan.setText("-");
            TextView hPasangan = findViewById(R.id.ETHPasangan);
            hPasangan.setText(bagian[7] + "");
        } else if(this.jenisKelamin.equals("perempuan")){
            TextView bPasangan = findViewById(R.id.ETBPasangan);
            if(furudA[6]>0){
                bPasangan.setText(furudA[6]+"/"+asal);
            }else bPasangan.setText("-");
            TextView hPasangan = findViewById(R.id.ETHPasangan);
            hPasangan.setText(bagian[6] + "");
        }

        TextView bAnakLaki = findViewById(R.id.ETBAnakLaki);
        if (asobah[0] > 0){
            bAnakLaki.setText("sisa");
        }else {
                bAnakLaki.setText("-");
        }
        TextView hAnakLaki = findViewById(R.id.ETHAnakLaki);
        hAnakLaki.setText(bagian[0]+"");

        TextView bAnakPerempuan = findViewById(R.id.ETBAnakPerempuan);
        if (asobah[1] > 0){
            bAnakPerempuan.setText("sisa");
        }else {
            if (furudA[1] > 0) {
                bAnakPerempuan.setText(furudA[1] + "/" + asal);
            } else {
                bAnakPerempuan.setText("-");
            }
        }
        TextView hAnakPerempuan = findViewById(R.id.ETHAnakPerempuan);
        hAnakPerempuan.setText(bagian[1]+"");

        TextView bCucuLaki = findViewById(R.id.ETBCucuLaki);
        if (asobah[2] > 0){
            bCucuLaki.setText("sisa");
        }else {
            bCucuLaki.setText("-");
        }
        TextView hCucuLaki = findViewById(R.id.ETHCucuLaki);
        hCucuLaki.setText(bagian[2]+"");

        TextView bCucuPerempuan = findViewById(R.id.ETBCucuPerempuan);
        if (asobah[3] > 0){
            bCucuPerempuan.setText("sisa");
        }else {
            if (furudA[3] > 0) {
                bCucuPerempuan.setText(furudA[3] + "/" + asal);
            } else {
                bCucuPerempuan.setText("-");
            }
        }
        TextView hCucuPerempuan = findViewById(R.id.ETHCucuPerempuan);
        hCucuPerempuan.setText(bagian[3]+"");

        TextView bSaudaraLaki = findViewById(R.id.ETBSaudaraLaki);
        if (asobah[12] > 0){
            bSaudaraLaki.setText("sisa");
        }else {
            bSaudaraLaki.setText("-");
        }
        TextView hSaudaraLaki = findViewById(R.id.ETHSaudaraLaki);
        hSaudaraLaki.setText(bagian[12]+"");

        TextView bSaudaraPerempuan = findViewById(R.id.ETBSaudaraPerempuan);
        if (asobah[13] > 0){
            bSaudaraPerempuan.setText("sisa");
        }else {
            if (furudA[13] > 0) {
                bSaudaraPerempuan.setText(furudA[13] + "/" + asal);
            } else {
                bSaudaraPerempuan.setText("-");
            }
        }
        TextView hSaudaraPerempuan = findViewById(R.id.ETHSaudaraPerempuan);
        hSaudaraPerempuan.setText(bagian[13]+"");

        TextView bSaudaraLakiBapak = findViewById(R.id.ETBSaudaraLakiBapak);
        if (asobah[14] > 0){
            bSaudaraLakiBapak.setText("sisa");
        }else {
            bSaudaraLakiBapak.setText("-");
        }
        TextView hSaudaraLakiBapak = findViewById(R.id.ETHSaudaraLakiBapak);
        hSaudaraLakiBapak.setText(bagian[14]+"");

        TextView bSaudaraPerempuanBapak = findViewById(R.id.ETBSaudaraPerempuanBapak);
        if (asobah[15] > 0){
            bSaudaraPerempuanBapak.setText("sisa");
        }else {
            if (furudA[15] > 0) {
                bSaudaraPerempuanBapak.setText(furudA[15] + "/" + asal);
            } else {
                bSaudaraPerempuanBapak.setText("-");
            }
        }
        TextView hSaudaraPerempuanBapak = findViewById(R.id.ETHSaudaraPerempuanBapak);
        hSaudaraPerempuanBapak.setText(bagian[15]+"");

        TextView bSaudaraLakiPerempuanibu = findViewById(R.id.ETBSaudaraLakiPerempuanibu);
        if(furudA[16]>0){
            bSaudaraLakiPerempuanibu.setText(furudA[16]+"/"+asal);
        }else bSaudaraLakiPerempuanibu.setText("-");
        TextView hSaudaraLakiPerempuanibu = findViewById(R.id.ETHSaudaraLakiPerempuanibu);
        hSaudaraLakiPerempuanibu.setText(bagian[16]+"");

        TextView bKeponakanLaki = findViewById(R.id.ETBKeponakanLaki);
        if (asobah[17] > 0){
            bKeponakanLaki.setText("sisa");
        }else {
            bKeponakanLaki.setText("-");
        }
        TextView hKeponakanLaki = findViewById(R.id.ETHKeponakanLaki);
        hKeponakanLaki.setText(bagian[17]+"");

        TextView bKeponakanLakiAyah = findViewById(R.id.ETBKeponakanLakiAyah);
        if (asobah[18] > 0){
            bKeponakanLakiAyah.setText("sisa");
        }else {
            bKeponakanLakiAyah.setText("-");
        }
        TextView hKeponakanLakiAyah = findViewById(R.id.ETHKeponakanLakiAyah);
        hKeponakanLakiAyah.setText(bagian[18]+"");

        TextView bPaman = findViewById(R.id.ETBPaman);
        if (asobah[19] > 0){
            bPaman.setText("sisa");
        }else {
            bPaman.setText("-");
        }
        TextView hPaman = findViewById(R.id.ETHPaman);
        hPaman.setText(bagian[19]+"");

        TextView bPamanBapak = findViewById(R.id.ETBPamanBapak);
        if (asobah[20] > 0){
            bPamanBapak.setText("sisa");
        }else {
            bPamanBapak.setText("-");
        }
        TextView hPamanBapak = findViewById(R.id.ETHPamanBapak);
        hPamanBapak.setText(bagian[20]+"");

        TextView bPutraPaman = findViewById(R.id.ETBPutraPaman);
        if (asobah[21] > 0){
            bPutraPaman.setText("sisa");
        }else {
            bPutraPaman.setText("-");
        }
        TextView hPutraPaman = findViewById(R.id.ETHPutraPaman);
        hPutraPaman.setText(bagian[21]+"");

        TextView bPutraPamanBapak = findViewById(R.id.ETBPutraPamanBapak);
        if (asobah[22] > 0){
            bPutraPamanBapak.setText("sisa");
        }else {
            bPutraPamanBapak.setText("-");
        }
        TextView hPutraPamanBapak = findViewById(R.id.ETHPutraPamanBapak);
        hPutraPamanBapak.setText(bagian[22]+"");

        TextView bPriaMerdeka = findViewById(R.id.ETBPriaMerdeka);
        if (asobah[23] > 0){
            bPriaMerdeka.setText("sisa");
        }else {
            bPriaMerdeka.setText("-");
        }
        TextView hPriaMerdeka = findViewById(R.id.ETHPriaMerdeka);
        hPriaMerdeka.setText(bagian[23]+"");

        TextView bWanitaMerdeka = findViewById(R.id.ETBWanitaMerdeka);
        if (asobah[24] > 0){
            bWanitaMerdeka.setText("sisa");
        }else {
            bWanitaMerdeka.setText("-");
        }
        TextView hWanitaMerdeka = findViewById(R.id.ETHWanitaMerdeka);
        hWanitaMerdeka.setText(bagian[24]+"");

    }

}
