package com.example.vitotamma.e_waris;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class home extends AppCompatActivity {
    ImageButton btn1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
         btn1= (ImageButton) findViewById(R.id.buku);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(getApplicationContext(),ilmu_waris.class);
               startActivity(intent);
            }
        });
    }
    public void kalkulator (View view){
        Intent intent = new Intent(this,awal.class);
        startActivity(intent);
    }
}
